﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitClassLibrary.Interfaces
{
    public interface IAngle
    {
        double Radians
        {
            get;
        }

        double Degrees
        {
            get;
        }
    }
}
